from contextlib import contextmanager
from datetime import datetime
from typing import Callable, Type

from fabric import Connection, task
from fabulous.color import ColorString, green, yellow
from invoke import Collection

HOST = '158.160.15.54'
USER = 'deploy'
PROJECT_DIR = '/home/deploy/todolist/'

ns = Collection()  # mandatory root namespace with special naming

prod = Collection('prod')
ns.add_collection(prod)

prod_env = Collection('env')
prod.add_collection(prod_env)


def collection_task(collection: Collection, name: str) -> Callable:
    """
    Decorator to ease declaration of task being nested in collection
    """

    def inner(f):
        f_as_task = task(f)
        collection.add_task(f_as_task, name)
        return f_as_task

    return inner


@collection_task(prod_env, 'set')
def set_env(_, key: str, value: str) -> None:
    with prod_connection() as c:
        _set_env(c, key, value)


@collection_task(prod_env, 'ask')
def ask_env_key(_, key: str):
    with prod_connection() as c:
        _ask_env(c, key)


@collection_task(prod_env, 'file')
def ask_env_file(_):
    with prod_connection() as c:
        _ask_env_file(c)


@collection_task(prod, 'reload')
def restart_containers(_):
    with prod_connection() as c, _restart_containers(c):
        pass


@contextmanager
def prod_connection():
    with Connection(HOST, user=USER) as c, c.cd(PROJECT_DIR):
        yield c


@contextmanager
def _restart_containers(c):
    _info(c, 'Stopping containers')
    c.run('docker-compose down')
    yield
    _info(c, 'Starting containers')
    c.run('docker-compose up -d')


def _set_env(c, key: str, value: str) -> None:
    env_backup_name = f'.env-{datetime.now().isoformat()}'

    _info(c, f'Dumping current .env to {env_backup_name}')
    c.run(f'cp .env {env_backup_name}')

    _info(c, f'Setting env {key}={value}')
    c.run(f'sed "/^{key}=/d" .env -i')  # deleted existing var if any
    c.run(f'echo "{key}={value}" >> .env')


def _ask_env(c, key: str) -> None:
    _info(c, f'Querying .env about {key}')
    res = c.run(f'grep {key} .env', warn=True)
    if res.exited == 1:
        _warning(c, '<not in .env>')


def _ask_env_file(c) -> None:
    _info(c, 'Querying .env file')
    c.run('cat .env', warn=True)


def _info(c, message):
    _log(green, f'[{c.host}] {message}...')


def _warning(c, message):
    _log(yellow, f'[{c.host}] {message}...')


def _log(color: Type[ColorString], message: str):
    print(color(message))  # noqa: T201
