from django.urls import path

from core.apps import CoreConfig
from core.views import LoginView, ProfileView, SignUpView, UpdatePasswordView

app_name = CoreConfig.name

urlpatterns = [
    path('signup', SignUpView.as_view(), name='signup'),
    path('login', LoginView.as_view(), name='login'),
    path('profile', ProfileView.as_view(), name='profile'),
    path('update_password', UpdatePasswordView.as_view(), name='update-password'),
]
