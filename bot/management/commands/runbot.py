from typing import Any, Callable

from django.core.management import BaseCommand
from pydantic import BaseModel

from bot.models import TgUser
from bot.tg.client import TgClient
from bot.tg.schemas import Message
from goals.models import BoardParticipant, Goal, GoalCategory


class FSM(BaseModel):
    next_handler: Callable[..., str]
    data: dict[str, Any] = {}


class Command(BaseCommand):
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)
        self.tg_client = TgClient()
        self.clients: dict[int, FSM] = {}

    def handle(self, *args: Any, **options: Any) -> None:
        offset = 0

        self.stdout.write(self.style.SUCCESS('Bot start handling...'))
        while True:
            res = self.tg_client.get_updates(offset=offset, allowed_updates='message')
            for item in res.result:
                offset = item.update_id + 1
                self.handle_message(item.message)

    def handle_message(self, msg: Message) -> None:
        tg_user, _ = TgUser.objects.get_or_create(chat_id=msg.chat.id)
        if not tg_user.is_verified:
            tg_user.update_verification_code()
            self.tg_client.send_message(tg_user.chat_id, text=f'Verification code: {tg_user.verification_code}')
        else:
            self.handle_authorized_user(tg_user, msg)

    def handle_authorized_user(self, tg_user: TgUser, msg: Message) -> None:
        if msg.text and msg.text.startswith('/'):
            match msg.text:
                case '/goals':
                    self.handle_get_goals_command(tg_user)
                case '/create':
                    self.handle_create_goal_command(tg_user)
                case '/cancel':
                    self.clients.pop(tg_user.chat_id, None)
                    self.tg_client.send_message(tg_user.chat_id, text='Canceled')
                case _:
                    self.tg_client.send_message(chat_id=tg_user.chat_id, text='Unknown command')
        elif tg_user.chat_id in self.clients:
            user_info: FSM = self.clients[tg_user.chat_id]
            text = user_info.next_handler(tg_user, msg, **user_info.data)
            self.tg_client.send_message(chat_id=tg_user.chat_id, text=text)
        else:
            available_commands = ('/goals', '/create', '/cancel')
            self.tg_client.send_message(
                chat_id=tg_user.chat_id, text='Available commands:\n' + '\n'.join(available_commands)
            )

    def handle_get_goals_command(self, tg_user: TgUser) -> None:
        if goals := Goal.objects.filter(
            category__board__participants__user=tg_user.user,
        ).exclude(status=Goal.Status.archived):
            text = '\n'.join([f'{goal.id}) {goal.title}' for goal in goals])
        else:
            text = 'No foals found'

        self.tg_client.send_message(chat_id=tg_user.chat_id, text=text)

    def handle_create_goal_command(self, tg_user: TgUser) -> None:
        if categories := GoalCategory.objects.filter(
            board__participants__user=tg_user.user,
            board__participants__role__in=[BoardParticipant.Role.owner, BoardParticipant.Role.writer],
        ).exclude(is_deleted=True):
            text = '\n'.join([f'{category.id}) {category.title}' for category in categories])
        else:
            text = 'No categories found'

        self.tg_client.send_message(chat_id=tg_user.chat_id, text=text)
        self.clients[tg_user.chat_id] = FSM(next_handler=self._get_category)

    def _get_category(self, tg_user: TgUser, msg: Message, **kwargs: Any) -> str:
        if not msg.text:
            raise NotImplementedError

        try:
            category = GoalCategory.objects.get(pk=msg.text)
        except GoalCategory.DoesNotExist:
            text = 'Category not found'
        else:
            self.clients[tg_user.chat_id] = FSM(next_handler=self._create_goal, data={'category': category})
            text = 'Set title'

        return text

    def _create_goal(self, tg_user: TgUser, msg: Message, **kwargs: Any) -> str:
        if not tg_user.user or not msg.text:
            raise NotImplementedError

        Goal.objects.create(user=tg_user.user, category=kwargs['category'], title=msg.text)
        self.clients.pop(tg_user.chat_id)
        return 'Goal created'
