from django.contrib import admin
from django.urls import reverse
from django.utils.html import format_html

from bot.models import TgUser


class IsVerifiedFilter(admin.SimpleListFilter):
    title = 'Verified'
    parameter_name = 'is_verified'

    def lookups(self, request, model_admin):  # type: ignore[no-untyped-def]
        return (
            ('Y', 'Yes'),
            ('N', 'No'),
        )

    def queryset(self, request, queryset):  # type: ignore[no-untyped-def]
        match self.value():
            case 'Y':
                queryset = queryset.exclude(user=None)
            case 'N':
                queryset = queryset.filter(user=None)
        return queryset


@admin.register(TgUser)
class TgUserAdmin(admin.ModelAdmin):
    list_display = ('chat_id', 'tg_user')
    readonly_fields = ['verification_code']
    list_filter = [IsVerifiedFilter]
    search_fields = ['chat_id']

    def tg_user(self, obj: TgUser) -> str | None:
        if user := obj.user:
            return format_html(
                "<a href='{url}'>{user_name}</a>",
                url=reverse('admin:core_user_change', kwargs={'object_id': user.id}),
                user_name=user.username,
            )

    tg_user.short_description = 'user'  # type: ignore[attr-defined]
