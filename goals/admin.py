from urllib.parse import urlencode

from django.contrib import admin
from django.urls import reverse
from django.utils.html import format_html

from goals.models import Board, BoardParticipant, Goal, GoalCategory, GoalComment


class ParticipantsInline(admin.TabularInline):
    model = BoardParticipant
    extra = 0


@admin.register(Board)
class BoardAdmin(admin.ModelAdmin):
    list_display = ('title', 'owner', 'categories', 'is_deleted')
    list_filter = ['is_deleted']
    search_fields = ['title']
    inlines = [ParticipantsInline]

    def owner(self, obj: Board) -> str:
        owner = obj.participants.get(role=BoardParticipant.Role.owner).user
        return format_html(
            "<a href='{url}'>{user_name}</a>",
            url=reverse('admin:core_user_change', kwargs={'object_id': owner.id}),
            user_name=owner.username,
        )

    def categories(self, obj: Board) -> str:
        # obj.categories.id
        return format_html(
            "<a href='{url}'>{categories_count}</a>",
            url=reverse('admin:goals_goalcategory_changelist') + '?' + urlencode({'board__in': obj.id}),
            categories_count=obj.categories.count(),
        )


@admin.register(GoalCategory)
class GoalCategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'author_link', 'goals', 'board_link')
    readonly_fields = ('created', 'updated')
    list_filter = ['is_deleted']
    search_fields = ['title']

    def author_link(self, obj: GoalCategory) -> str:
        return format_html(
            "<a href='{url}'>{user_name}</a>",
            url=reverse('admin:core_user_change', kwargs={'object_id': obj.user_id}),
            user_name=obj.user.username,
        )

    def board_link(self, obj: GoalCategory) -> str:
        board = obj.board
        return format_html(
            "<a href='{url}'>{board_title}</a>",
            url=reverse('admin:goals_board_change', kwargs={'object_id': board.id}),
            board_title=board.title,
        )

    def goals(self, obj: GoalCategory) -> str:
        return format_html(
            "<a href='{url}'>{goals_count}</a>",
            url=reverse('admin:goals_goal_changelist') + '?' + urlencode({'category__in': obj.id}),
            goals_count=obj.goal_set.count(),
        )

    author_link.short_description = 'Author'  # type: ignore[attr-defined]
    goals.short_description = 'Goals'  # type: ignore[attr-defined]
    board_link.short_description = 'Board'  # type: ignore[attr-defined]


class CommentsInline(admin.StackedInline):
    model = GoalComment
    extra = 0


@admin.register(Goal)
class GoalAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'author_link', 'category_link')
    search_fields = ('title', 'description')
    readonly_fields = ('created', 'updated')
    list_filter = ('status', 'priority')
    inlines = [CommentsInline]

    def author_link(self, obj: Goal) -> str:
        return format_html(
            "<a href='{url}'>{user_name}</a>",
            url=reverse('admin:core_user_change', kwargs={'object_id': obj.user_id}),
            user_name=obj.user.username,
        )

    def category_link(self, obj: Goal) -> str:
        return format_html(
            "<a href='{url}'>{category_title}</a>",
            url=reverse('admin:goals_goalcategory_change', kwargs={'object_id': obj.category_id}),
            category_title=obj.category.title,
        )

    author_link.short_description = 'Author'  # type: ignore[attr-defined]
    category_link.short_description = 'Category'  # type: ignore[attr-defined]
