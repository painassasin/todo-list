from django.db import models

from core.models import User
from todolist.models import BaseModel


class Board(BaseModel):
    title = models.CharField(max_length=255)
    is_deleted = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'Board'
        verbose_name_plural = 'Boards'

    def __str__(self) -> str:
        return self.title


class BoardParticipant(BaseModel):
    class Role(models.IntegerChoices):
        owner = 1, 'Owner'
        writer = 2, 'Writer'
        reader = 3, 'Reader'

    board = models.ForeignKey(Board, on_delete=models.PROTECT, related_name='participants')
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='participants')
    role = models.PositiveSmallIntegerField(choices=Role.choices, default=Role.owner)

    class Meta:
        unique_together = ('board', 'user')
        verbose_name = 'Participant'
        verbose_name_plural = 'Participants'

    editable_roles: list[tuple[int, str]] = Role.choices[1:]


class GoalCategory(BaseModel):
    title = models.CharField(max_length=255)
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    is_deleted = models.BooleanField(default=False)
    board = models.ForeignKey(Board, on_delete=models.PROTECT, related_name='categories')

    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'

    def __str__(self) -> str:
        return self.title


class Goal(BaseModel):
    class Status(models.IntegerChoices):
        to_do = 1, 'To do'
        in_progress = 2, 'In progress'
        done = 3, 'Done'
        archived = 4, 'Archived'

    class Priority(models.IntegerChoices):
        low = 1, 'Low'
        medium = 2, 'Medium'
        high = 3, 'High'
        critical = 4, 'Critical'

    title = models.CharField(max_length=255)
    description = models.TextField(blank=True)
    category = models.ForeignKey(to=GoalCategory, on_delete=models.PROTECT)
    due_date = models.DateField(null=True, blank=True)
    user = models.ForeignKey(to=User, on_delete=models.PROTECT)
    status = models.PositiveSmallIntegerField(choices=Status.choices, default=Status.to_do)
    priority = models.PositiveSmallIntegerField(choices=Priority.choices, default=Priority.medium)

    class Meta:
        verbose_name = 'Goal'
        verbose_name_plural = 'Goals'

    def __str__(self) -> str:
        return self.title


class GoalComment(BaseModel):
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    goal = models.ForeignKey(Goal, on_delete=models.CASCADE)
    text = models.TextField()

    def __str__(self) -> str:
        return self.text

    class Meta:
        verbose_name = 'Comment'
        verbose_name_plural = 'Comments'
