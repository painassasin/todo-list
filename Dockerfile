FROM python:3.10.9-slim as python_base

ENV PIP_DISABLE_PIP_VERSION_CHECK=on \
    PATH="/opt/.venv/bin:$PATH"

WORKDIR /opt/todolist

ENTRYPOINT ["/bin/bash", "entrypoint.sh"]

EXPOSE 8000

FROM python_base AS compile_base

RUN pip install --no-cache-dir "poetry==1.5.1"

WORKDIR /tmp

COPY poetry.lock pyproject.toml ./

RUN poetry export --with dev -f requirements.txt -o /tmp/requirements.dev.txt && \
    poetry export -f requirements.txt -o /tmp/requirements.release.txt

RUN python -m venv /opt/.venv

FROM compile_base as release_base
RUN python -m pip install -r requirements.release.txt --no-cache-dir

FROM release_base AS dev_base
RUN python -m pip install -r requirements.dev.txt --no-cache-dir

FROM python_base as release_image
COPY --from=release_base /opt/.venv /opt/.venv
COPY . .
CMD ["gunicorn", "todolist.wsgi", "-w", "4", "-b", "0.0.0.0:8000"]

FROM python_base as dev_image
COPY --from=dev_base /opt/.venv /opt/.venv
COPY . .
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
