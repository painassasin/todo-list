import random
from unittest.mock import ANY

import pytest
from django.urls import reverse
from rest_framework import status
from rest_framework.fields import DateTimeField

from goals.models import Board, BoardParticipant, Goal


@pytest.mark.django_db()
class TestBoardCreateView:
    url = reverse('goals:create-board')
    data = {'title': 'New board'}

    def test_auth_required(self, client):
        response = client.post(self.url, data=self.data)
        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_request_user_became_board_owner(self, auth_client, user):
        response = auth_client.post(self.url, data=self.data)

        assert response.status_code == status.HTTP_201_CREATED
        new_board = Board.objects.get()
        assert response.json() == _serialize_response(new_board)
        assert BoardParticipant.objects.get(user=user, board=new_board, role=BoardParticipant.Role.owner)

    @pytest.mark.django_db(transaction=True)
    def test_board_creates_in_transaction(self, auth_client, django_assert_num_queries):
        with django_assert_num_queries(6):
            # select django_session
            # select core_user
            # begin
            # insert goal
            # insert board_participant
            # commit
            response = auth_client.post(self.url, data=self.data)

        assert response.status_code == status.HTTP_201_CREATED


@pytest.mark.django_db()
class TestBoardListView:
    url = reverse('goals:board-list')

    def test_auth_required(self, client):
        response = client.get(self.url)
        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_boards_sorted_by_title(self, auth_client, user, board_factory):
        boards = board_factory.create_batch(5, with_owner=user)

        response = auth_client.get(self.url)

        assert response.status_code == status.HTTP_200_OK
        assert response.json() == [_serialize_response(board) for board in sorted(boards, key=lambda x: x.title)]

    @pytest.mark.parametrize(
        'participant_role',
        [BoardParticipant.Role.writer, BoardParticipant.Role.reader],
        ids=['writer', 'reader'],
    )
    def test_get_boards_by_any_participants(  # noqa: CFQ002
        self,
        auth_client,
        another_user,
        board_factory,
        board_participant_factory,
        participant_role,
        user,
    ):
        board = board_factory.create(with_owner=another_user)
        board_participant_factory.create(user=user, board=board, role=participant_role)

        response = auth_client.get(self.url)

        assert response.status_code == status.HTTP_200_OK
        assert response.json() == [_serialize_response(board)]

    def test_failed_to_retrieve_deleted_boards(self, auth_client, board_factory, user):
        board_factory.create(with_owner=user, is_deleted=True)

        response = auth_client.get(self.url)

        assert response.status_code == status.HTTP_200_OK
        assert response.json() == []

    def test_queries_count(self, auth_client, user, another_user, board_factory, django_assert_num_queries):
        board_factory.create_batch(2, with_owner=user)
        board_factory.create_batch(3, with_owner=another_user)

        with django_assert_num_queries(3):
            # select django_session
            # select core_user
            # select boards
            response = auth_client.get(self.url)

        assert response.status_code == status.HTTP_200_OK
        assert len(response.json()) == 2

    def test_pagination(self, auth_client, user, board_factory, django_assert_num_queries):
        boards = board_factory.create_batch(10, with_owner=user)

        with django_assert_num_queries(4):
            # select django_session
            # select core_user
            # select boards count
            # select boards
            response = auth_client.get(self.url, data={'limit': 5})

        assert response.status_code == status.HTTP_200_OK
        assert response.json() == {
            'count': 10,
            'next': ANY,
            'previous': None,
            'results': [_serialize_response(board) for board in sorted(boards, key=lambda x: x.title)[:5]],
        }


@pytest.mark.django_db()
class TestRetrieveBoardView:
    @pytest.fixture(autouse=True)
    def setup(self, board_participant) -> None:  # noqa: PT004
        self.url = self.get_url(board_participant.board)

    @staticmethod
    def get_url(board: Board) -> str:
        return reverse('goals:board-detail', kwargs={'pk': board.pk})

    def test_auth_required(self, client):
        response = client.get(self.url)
        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_user_not_participant(self, client, another_user):
        client.force_login(another_user)
        response = client.get(self.url)
        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_query_count(self, auth_client, board_participant_factory, board, faker, django_assert_num_queries):
        board_participant_factory.create_batch(
            size=10,
            board=board,
            role=faker.random_element([BoardParticipant.Role.reader, BoardParticipant.Role.writer]),
        )

        with django_assert_num_queries(6):
            # select django_session
            # select core_user
            # select goals_board
            # select board_participants
            # select core_users
            # select goals_board_participant
            response = auth_client.get(self.url)

        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.parametrize('role', BoardParticipant.Role, ids=[role.name for role in BoardParticipant.Role])
    def test_have_to_retrieve_board_by_any_role(  # noqa: CFQ002
        self, client, user, another_user, role, board, board_participant_factory
    ):
        if role != BoardParticipant.Role.owner:
            user = another_user
            board_participant_factory.create(board=board, user=another_user, role=role)
        client.force_login(user)

        response = client.get(self.url)

        assert response.status_code == status.HTTP_200_OK

    def test_failed_to_retrieve_deleted_board(self, auth_client, board):
        board.is_deleted = True
        board.save(update_fields=['is_deleted'])

        response = auth_client.get(self.url)

        assert response.status_code == status.HTTP_404_NOT_FOUND


@pytest.mark.django_db(transaction=True)
class TestPartialUpdateBoardView:
    @pytest.fixture(autouse=True)
    def setup(self, board_participant) -> None:  # noqa: PT004
        self.url = self.get_url(board_participant.board)

    @staticmethod
    def get_url(board: Board) -> str:
        return reverse('goals:board-detail', kwargs={'pk': board.pk})

    def test_auth_required(self, client):
        response = client.patch(self.url)
        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_update_board_title_by_owner(self, auth_client, board, faker):
        new_board_title = faker.sentence()

        response = auth_client.patch(self.url, {'title': new_board_title})

        assert response.status_code == status.HTTP_200_OK
        board.refresh_from_db()
        assert board.title == new_board_title
        assert response.json() == _serialize_response(board, with_participants=True, title=new_board_title)

    @pytest.mark.parametrize(
        'participant_role',
        [BoardParticipant.Role.writer, BoardParticipant.Role.reader],
        ids=['writer', 'reader'],
    )
    def test_failed_to_update_board_by_not_owner(  # noqa: CFQ002
        self, client, another_user, participant_role, faker, board_participant_factory, board
    ):
        board_participant_factory.create(board=board, user=another_user, role=participant_role)
        client.force_login(another_user)

        response = client.patch(self.url, {'title': faker.sentence()})

        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_owner_failed_to_change_his_role(self, auth_client, user):
        response = auth_client.patch(
            self.url, data={'participants': [{'user': user.username, 'role': _get_random_editable_role()}]}
        )

        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert response.json() == {'participants': [{'user': ['Failed to change your role']}]}

    def test_failed_to_set_many_owners(self, auth_client, another_user):
        response = auth_client.patch(
            self.url,
            data={
                'participants': [
                    {'user': another_user.username, 'role': BoardParticipant.Role.owner},
                ]
            },
        )

        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert response.json() == {  # noqa: ECE001
            'participants': [{'role': [f'"{BoardParticipant.Role.owner.value}" is not a valid choice.']}]
        }

    def test_failed_to_set_many_roles_for_one_user(self, auth_client, another_user):
        response = auth_client.patch(
            self.url,
            data={
                'participants': [
                    {'user': another_user.username, 'role': BoardParticipant.Role.writer},
                    {'user': another_user.username, 'role': BoardParticipant.Role.reader},
                ]
            },
        )

        assert response.status_code == status.HTTP_200_OK
        assert BoardParticipant.objects.get(user=another_user).role == BoardParticipant.Role.writer

    def test_add_board_participant(self, auth_client, another_user):
        response = auth_client.patch(
            self.url,
            data={
                'participants': [
                    {'user': another_user.username, 'role': _get_random_editable_role()},
                ]
            },
        )

        assert response.status_code == status.HTTP_200_OK
        assert BoardParticipant.objects.count() == 2

    def test_delete_board_participant(self, auth_client, user, another_user, board_participant_factory, board):
        board_participant_factory.create(board=board, user=another_user, role=_get_random_editable_role())

        response = auth_client.patch(self.url, {'participants': []})

        assert response.status_code == status.HTTP_200_OK
        assert BoardParticipant.objects.get(board=board).user == user

    def test_change_board_participant_role(self, auth_client, another_user, board_participant_factory, board):
        board_participant_factory.create(board=board, user=another_user, role=BoardParticipant.Role.writer)

        response = auth_client.patch(
            self.url, data={'participants': [{'role': BoardParticipant.Role.reader, 'user': another_user.username}]}
        )

        assert response.status_code == status.HTTP_200_OK
        assert BoardParticipant.objects.get(user=another_user).role == BoardParticipant.Role.reader


@pytest.mark.django_db()
class TestDeleteBoard:
    @pytest.fixture(autouse=True)
    def setup(self, board_participant) -> None:  # noqa: PT004
        self.url = self.get_url(board_participant.board)

    @staticmethod
    def get_url(board: Board) -> str:
        return reverse('goals:board-detail', kwargs={'pk': board.pk})

    def test_auth_required(self, client):
        response = client.delete(self.url)
        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_failed_to_delete_foreign_board(self, auth_client, another_user, board_factory):
        foreign_board = board_factory.create(with_owner=another_user)
        url = self.get_url(foreign_board)

        response = auth_client.delete(url)

        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_delete_board_with_goals_and_categories(self, auth_client, goal, goal_category, board):
        response = auth_client.delete(self.url)

        assert response.status_code == status.HTTP_204_NO_CONTENT
        goal.refresh_from_db(fields=['status'])
        goal_category.refresh_from_db(fields=['is_deleted'])
        board.refresh_from_db(fields=['is_deleted'])
        assert goal.status == Goal.Status.archived
        assert goal_category.is_deleted is True
        assert board.is_deleted is True


def _get_random_editable_role() -> BoardParticipant.Role:
    role_value, _ = random.choice(BoardParticipant.editable_roles)
    return BoardParticipant.Role(role_value)


def _serialize_response(board: Board, with_participants: bool = False, **kwargs) -> dict:
    data = {
        'id': board.id,
        'created': DateTimeField().to_representation(board.created),
        'updated': DateTimeField().to_representation(board.updated),
        'title': board.title,
        'is_deleted': False,
    }
    if with_participants:
        data['participants'] = [
            {
                'id': participant.id,
                'board': participant.board_id,
                'created': DateTimeField().to_representation(participant.created),
                'updated': DateTimeField().to_representation(participant.updated),
                'role': participant.role,
                'user': participant.user.username,
            }
            for participant in board.participants.prefetch_related('user')
        ]

    data |= kwargs
    return data
