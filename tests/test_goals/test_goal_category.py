import pytest
from django.urls import reverse
from rest_framework import status
from rest_framework.fields import DateTimeField

from goals.models import BoardParticipant, GoalCategory
from tests.test_goals.factories import CreateGoalCategoryRequest


@pytest.mark.django_db()
class TestCreateGoalCategoryView:
    # select session
    # select user
    # select board
    # select participants (exists)
    # insert category
    queries_count: int = 5
    url = reverse('goals:create-category')

    def test_auth_required(self, client):
        response = client.post(self.url)
        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_failed_to_create_category_by_not_board_participant(self, client, board, another_user):
        client.force_login(another_user)
        data = CreateGoalCategoryRequest(board=board.id)

        response = client.post(self.url, data=data)

        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_failed_to_create_category_by_reader(self, client, board, another_user):
        BoardParticipant.objects.create(board=board, user=another_user, role=BoardParticipant.Role.reader)
        client.force_login(another_user)
        data = CreateGoalCategoryRequest(board=board.id)

        response = client.post(self.url, data=data)

        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_failed_to_create_category_by_writer(self, client, board, another_user):
        BoardParticipant.objects.create(board=board, user=another_user, role=BoardParticipant.Role.writer)
        client.force_login(another_user)
        data = CreateGoalCategoryRequest(board=board.id)

        response = client.post(self.url, data=data)

        assert response.status_code == status.HTTP_201_CREATED
        new_category = GoalCategory.objects.get()
        assert response.json() == _serialize_response(new_category)

    def test_failed_to_create_category_by_owner(self, auth_client, django_assert_num_queries, board, user):
        BoardParticipant.objects.create(board=board, user=user, role=BoardParticipant.Role.owner)
        data = CreateGoalCategoryRequest(board=board.id)

        with django_assert_num_queries(self.queries_count):
            response = auth_client.post(self.url, data=data)

        assert response.status_code == status.HTTP_201_CREATED
        new_category = GoalCategory.objects.get()
        assert response.json() == _serialize_response(new_category)


@pytest.mark.django_db()
class TestRetrieveGoalCategory:
    @pytest.fixture(autouse=True)
    def setup(self, goal_category) -> None:  # noqa: PT004
        self.url = self.get_url(goal_category)

    @staticmethod
    def get_url(category: GoalCategory) -> str:
        return reverse('goals:category-detail', kwargs={'pk': category.pk})

    def test_auth_required(self, client):
        response = client.get(self.url)
        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_user_not_board_participant(self, auth_client):
        response = auth_client.get(self.url)
        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_deleted_category(self, auth_client, goal_category):
        goal_category.is_deleted = True
        goal_category.save(update_fields=['is_deleted'])

        response = auth_client.get(self.url)

        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.parametrize('user_role', BoardParticipant.Role, ids=[role.name for role in BoardParticipant.Role])
    def test_any_participant_have_to_retrieve_category(self, auth_client, board_participant, goal_category, user_role):
        board_participant.role = user_role
        board_participant.save()

        response = auth_client.get(self.url)

        assert response.status_code == status.HTTP_200_OK
        assert response.json() == _serialize_response(goal_category, with_user=True)


def _serialize_response(goal_category: GoalCategory, with_user=False, **kwargs):
    data = {
        'id': goal_category.id,
        'created': DateTimeField().to_representation(goal_category.created),
        'updated': DateTimeField().to_representation(goal_category.updated),
        'title': goal_category.title,
        'is_deleted': False,
        'board': goal_category.board_id,
    }
    if with_user:
        data['user'] = {
            'id': goal_category.user.id,
            'first_name': goal_category.user.first_name,
            'last_name': goal_category.user.last_name,
            'email': goal_category.user.email,
            'username': goal_category.user.username,
        }
    data |= kwargs
    return data
