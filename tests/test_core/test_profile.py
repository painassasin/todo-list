import pytest
from django.contrib.auth import get_user
from django.urls import reverse
from rest_framework import status

from core.models import User


@pytest.fixture()
def get_url(request) -> None:  # noqa: PT004
    request.cls.url = reverse('core:profile')


@pytest.mark.django_db()
@pytest.mark.usefixtures('get_url')
class TestGetProfileView:
    def test_auth_required(self, client):
        response = client.get(self.url)
        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_returns_request_user(self, auth_client):
        response = auth_client.get(self.url)
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == self._serialize_response(user=get_user(auth_client))

    @staticmethod
    def _serialize_response(user: User, **kwargs) -> dict:
        data = {
            'id': user.id,
            'username': user.username,
            'first_name': user.first_name,
            'last_name': user.last_name,
            'email': user.last_name,
        }
        data |= kwargs
        return data


@pytest.mark.django_db()
@pytest.mark.usefixtures('get_url')
class TestLogoutView:
    def test_auth_required(self, client):
        response = client.delete(self.url)
        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_user_not_deleted_on_logout(self, auth_client, user):
        response = auth_client.delete(self.url)
        assert response.status_code == status.HTTP_204_NO_CONTENT
        assert User.objects.filter(id=user.id).exists()

    def test_user_logged_out(self, auth_client):
        response = auth_client.delete(self.url)
        assert response.status_code == status.HTTP_204_NO_CONTENT
        assert not get_user(auth_client).is_authenticated


@pytest.mark.django_db()
@pytest.mark.usefixtures('get_url')
class TestUpdateProfileView:
    def test_auth_required(self, client):
        response = client.patch(self.url)
        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_update_username_on_existing(self, auth_client, user_factory):
        another_user = user_factory.create()

        response = auth_client.patch(self.url, data={'username': another_user.username})

        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert response.json() == {'username': ['A user with that username already exists.']}

    def test_failed_to_update_password(self, client, user_factory, faker):
        password = faker.password()
        user = user_factory.create(password=password)
        client.force_login(user)

        response = client.patch(self.url, data={'password': faker.password()})

        assert response.status_code == status.HTTP_200_OK
        assert response.json()
        user.refresh_from_db()
        assert user.check_password(password)

    @pytest.mark.parametrize(
        ('model_field', 'faker_field'),
        [('username', 'user_name'), ('first_name', 'first_name'), ('last_name', 'last_name'), ('email', 'email')],
        ids=['username', 'first_name', 'last_name', 'email'],
    )
    def test_update_user_fields(self, auth_client, user, faker, model_field, faker_field):
        new_field_value = getattr(faker, faker_field)()

        response = auth_client.patch(self.url, data={model_field: new_field_value})

        assert response.status_code == status.HTTP_200_OK
        assert response.json() == self._serialize_response(user, **{model_field: new_field_value})

    @staticmethod
    def _serialize_response(user: User, **kwargs) -> dict:
        data = {
            'id': user.id,
            'username': user.username,
            'first_name': user.first_name,
            'last_name': user.last_name,
            'email': user.last_name,
        }
        data |= kwargs
        return data
